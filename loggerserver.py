#!flask/bin/python
from flask import Flask, jsonify, make_response, request, abort
from sqlitedict import SqliteDict
import time, datetime

app = Flask(__name__)

#measurements = [] #USE A PROPER DATA STORE - THIS IS le GROSS
measurements = SqliteDict('./db.sqlite')

@app.route('/dht/api/v1.0/measurements', methods=['GET'])
def get_measurements():
	return jsonify({'measurements': {str(datetime.datetime.fromtimestamp(float(x))): measurements[x] for x in measurements.keys()}})

#@app.route('/dht/api/v1.0/measurements/sensor/<string:sensor_id>', methods=['GET'])
#def get_measurements_by_sensor(sensor_id):
#	measurement_set = [measurement for measurement in measurements if measurement['sensor_id'] == sensor_id]
#	if len(measurement_set) == 0:
#		abort(404)
#	return jsonify({'measurements': measurement_set})
#


@app.route('/dht/api/v1.0/measurements', methods=['POST'])
def create_measurement():

	if not request.json or not 'sensor_id' in request.json:
		abort(400)
	print("got measurement from")
	m = parse_measurement(request)
	measurements[int(time.time())] = m
	return jsonify({'measurement': m}), 201

#   measurements = {'sensor_id':'macaddresshereplusint','temp':sensor.temperature(),'humidity':sensor.humidity()}



@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': 'Not found'}), 404)


def parse_measurement(r):
	return {
		'sensor_id': r.json['sensor_id'],
		'temp': r.json.get('temp'),
		'humidity': r.json.get('humidity'),
	}
	

if __name__ == '__main__':
	app.run(host = '0.0.0.0',port=8888, debug=True)
